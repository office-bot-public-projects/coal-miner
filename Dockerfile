##################################################################
# Stage 1 - Build
##################################################################
FROM node:14-alpine as build

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./
USER node
RUN npm ci --only=production
COPY --chown=node:node . .
##################################################################
# Stage 2 - Deploy
##################################################################
FROM node:14-alpine as deploy
WORKDIR /home/node/app
COPY --from=build /home/node/app .
RUN apk add --no-cache tini
RUN npm uninstall -g npm
# Tini is now available at /sbin/tini
ENTRYPOINT ["/sbin/tini", "--"]

EXPOSE 3000
CMD [ "node", "index.js" ]