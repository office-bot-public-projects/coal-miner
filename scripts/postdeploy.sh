#!/bin/sh
curl -L -H "Content-Type: application/json" \
-H "PRIVATE-TOKEN: $ACCESS_TOKEN" \
-d '{ "source_branch": "master", "target_branch": "develop", "title" : "[ci skip] Post deployment merge: '"'$CI_PROJECT_ID'"'"}' \
https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests
echo "Merge request generated: https://gitlab.com/office-bot-api/coal-miner/-/merge_requests"