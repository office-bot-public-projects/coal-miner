const express = require('express');
const app = express();
const path = require('path');

app.get('/', function primaryHandler(req, res) {
  res.sendFile(path.resolve(__dirname+'/public/index.html'));
});

const port = 3000;
app.listen(port, (err) => {
  if (err) {
    throw err;
  }
  console.log('Mining on port 3000');
});